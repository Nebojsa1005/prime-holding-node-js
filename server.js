const express = require('express')
const mongoose = require('mongoose')
const vehicles = require('./routes/vehicles.js')
const vehiclesTypes = require('./routes/vehiclesType.js')
const customers = require('./routes/customers.js')
const rentalEvent = require('./routes/rentalEvent.js')
const app = express()


mongoose.connect('mongodb://localhost/nodeJs', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('CONNECTED TO DATABESE'))
    .catch(() => console.log('ERROR!!!'))

mongoose.set('useFindAndModify', false)

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// 
app.use('/', vehicles)
app.use('/vehicleTypes', vehiclesTypes)
app.use('/customers', customers)
app.use('/rentalEvent', rentalEvent)

app.listen(3000, () => {
    console.log('App Started!!!')
})
