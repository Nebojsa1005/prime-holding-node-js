const mongoose = require('mongoose')
const VehicleType = require('../models/vehicleType.js')
const Vehicle = require('../models/vehicle.js')
const Customer = require('../models/customer.js')
const RentalEvent = require('../models/rentalEvent.js')


mongoose.connect('mongodb://localhost/nodeJs', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('CONNECTED TO SEED DATABESE'))
    .catch(() => console.log('ERROR!!!'))


const seedVehicleType = async () => {
    await VehicleType.deleteMany({})
    const sedan = new VehicleType({
        createdAt: new Date,
        name: 'Sedan'
    })
    const coupe = new VehicleType({
        createdAt: new Date,
        name: 'Coupe'
    })
    const hatchback = new VehicleType({
        createdAt: new Date,
        name: 'Hatchback'
    })
    const sportCar = new VehicleType({
        createdAt: new Date,
        name: 'Sport Car'
    })

    await sedan.save()
    await coupe.save()
    await hatchback.save()
    await sportCar.save()
}

const seedDB = async () => {
    // it shows null as vehicleType field,
    //  this way so I pulled out seedVehicleType function and called if from an async IIF function and it works

    // SEEDING VEHICLE TYPES
    const seedVehicleType = async () => {
        await VehicleType.deleteMany({})
        const sedan = new VehicleType({
            createdAt: new Date,
            name: 'Sedan'
        })
        const coupe = new VehicleType({
            createdAt: new Date,
            name: 'Coupe'
        })
        const hatchback = new VehicleType({
            createdAt: new Date,
            name: 'Hatchback'
        })
        const sportCar = new VehicleType({
            createdAt: new Date,
            name: 'Sport Car'
        })

        await sedan.save()
        await coupe.save()
        await hatchback.save()
        await sportCar.save()
    }

    // SEEDING CUSTOMERS
    seedCustomers = async ({ name, email, phone, rentedVehicles }) => {
        await Customer.deleteMany({})
        let person = new Customer({
            createdAt: new Date,
            fullName: name,
            emailAddress: email,
            phoneNumber: phone,
            rentedVehicles: rentedVehicles
        })
        await person.save()
    }

    // SEEDING VEHICLES
    const seedVehicles = async ({ brand, model, constructionDate, fuelType, numberOfSeats, pricePerDay, picture, count }, vType) => {
        await Vehicle.deleteMany({})
        const car = new Vehicle({
            model: model,
            brand: brand,
            constructionDate: constructionDate,
            fuelType: fuelType,
            numberOfSeats: numberOfSeats,
            pricePerDay: pricePerDay,
            picture: picture,
            count: count,
        })

        car.vehicleType = await VehicleType.findOne({ name: vType })
        await car.save()
    }

    // SEEDING RENTAL EVENTS
    const seedRentalEvents = async ({ startDate, endDate, vehicleModel, customer }) => {
        await RentalEvent.deleteMany({})
        const wantedCustomer = await Customer.findOne({ fullName: customer })
        const wantedVehicle = await Vehicle.findOne({ model: vehicleModel })
        wantedVehicle.vehicleType = await VehicleType.findOne({ _id: wantedVehicle.vehicleType })
        let discount = 0
        let customerBonusPeriod = null

        const rentalEvent = new RentalEvent({
            startDate: new Date(startDate),
            endDate: new Date(endDate),
            vehicle: wantedVehicle,
            customer: wantedCustomer
        })

        let daysToBeUsed = new Date(
            rentalEvent.endDate.getTime() - rentalEvent.startDate.getTime()
        )

        if (wantedCustomer.lastRent) {
            let timeDifference = new Date(new Date().getTime() - new Date(wantedCustomer.lastRent).getTime())
            // 1 day has 86400000 miliseconds
            customerBonusPeriod = timeDifference.getTime() / 86400000 < 60 && wantedCustomer.rentedVehicles > 3
        } else {
            wantedCustomer.lastRent = rentalEvent.startDate
        }

        if (customerBonusPeriod) {
            discount = 15
        } else if (new Date(daysToBeUsed).getDate() > 10) {
            discount = 10
        } else if (new Date(daysToBeUsed).getDate() > 5) {
            discount = 7
        } else if (new Date(daysToBeUsed).getDate() > 3) {
            discount = 5
        }
        rentalEvent.price = (rentalEvent.vehicle.pricePerDay - (rentalEvent.vehicle.pricePerDay * discount) / 100).toFixed(2)
        wantedCustomer.lastRent = rentalEvent.startDate
        wantedCustomer.rentedVehicles++
        wantedVehicle.count--

        await wantedCustomer.save()
        await wantedVehicle.save()
        await rentalEvent.save()

    }
    // seedVehicleType()
    seedCustomers({
        name: 'Novak Djokovic',
        email: 'novak@novakNovak.com',
        phone: 063555333,
        rentedVehicles: 3
    })
    seedCustomers({
        name: 'Keisy Blase',
        email: 'keisyBlase@gmail.com',
        phone: 061777789,
        rentedVehicles: 10
    })
    seedVehicles({
        model: 'X4',
        brand: 'BMW',
        constructionDate: 2017,
        fuelType: 'Petrol',
        numberOfSeats: 2,
        pricePerDay: 45,
        picture: 'https://images.unsplash.com/photo-1517153295259-74eb0b416cee?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ejR8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
        count: 4
    }, 'Sport Car')
    seedVehicles({
        model: 'C Class',
        brand: 'Mercedes Benz',
        constructionDate: 2012,
        fuelType: 'Diesel',
        numberOfSeats: 5,
        pricePerDay: 45,
        picture: 'https://images.unsplash.com/photo-1599484527332-36a69e49a139?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8YyUyMGNsYXNzfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
        count: 4
    }, 'Coupe')
    seedRentalEvents({
        startDate: new Date(),
        endDate: new Date('04 18 2021'),
        vehicleModel: 'X4',
        customer: 'Novak Djokovic'
    })

}
// seedDB()


(async () => {
    await seedVehicleType()
    await seedDB()
})()