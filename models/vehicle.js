const mongoose = require('mongoose')
const Schema = mongoose.Schema

const VehicleSchema = new Schema({
    id: {
        type: Schema.Types.ObjectId,
    },
    createdAt: {
        type: Date,
        required: true,
        default: new Date
    },
    updatedAt: {
        type: Date,
    },
    vehicleType: {
        type: Schema.Types.ObjectId,
        ref: 'VehicleType',
        // required: true
    },
    brand: {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    constructionDate: {
        type: Number,
        required: true
    },
    fuelType: {
        type: String,
        required: true
    },
    numberOfSeats: {
        type: Number,
        required: true
    },
    picture: {
        type: String,
        required: true
    },
    pricePerDay: {
        type: Number,
        required: true
    },
    count: {
        type: Number,
        required: true
    },
})

const Vehicle = mongoose.model('Vehicle', VehicleSchema)

module.exports = Vehicle