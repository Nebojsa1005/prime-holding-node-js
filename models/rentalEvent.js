const mongoose = require('mongoose')
const Schema = mongoose.Schema

const rentalEventSchema = new Schema({
    id: {
        type: Schema.Types.ObjectId, ref: 'RentalEvent'
    },
    startDate: {
        type: Date,
        required: true,
        default: new Date
    },
    endDate: {
        type: Date,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    vehicle: {
        type: Schema.Types.ObjectId,
        ref: 'Vehicle',
        required: true
    },
    customer: {
        type: Schema.Types.ObjectId,
        ref: 'Customer',
        required: true
    },
})

module.exports = mongoose.model('RentalEvent', rentalEventSchema)