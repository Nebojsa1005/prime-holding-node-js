const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CustomerSchema = new Schema({
    id: {
        type: Schema.Types.ObjectId
    },
    createdAt: {
        type: Date,
        required: true
    },
    updatedAt: {
        type: Date,
    },
    fullName: {
        type: String,
        required: true
    },
    emailAddress: {
        type: String,
        required: true
    },
    phoneNumber: {
        type: String,
        required: true
    },
    rentedVehicles: {
        type: Number,
        default: 0
    },
    lastRent: Date

})

module.exports = mongoose.model('Customer', CustomerSchema)