const mongoose = require('mongoose')
const Schema = mongoose.Schema

const VehicleTypeSchema = new Schema({
    createdAt: {
        type: Date,
        required: true
    },
    updatedAt: {
        type: Date,
    },
    name: {
        type: String,
        required: true
    },
})

const VehicleType = mongoose.model('VehicleType', VehicleTypeSchema)

module.exports = VehicleType