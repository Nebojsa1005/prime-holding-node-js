const express = require('express')
const RentalEvent = require('../models/rentalEvent.js')
const Vehicle = require('../models/vehicle.js')
const VehicleType = require('../models/vehicleType.js')
const Customer = require('../models/customer.js')

const router = express.Router()

// GET LIST OF ALL RENTAL EVENTS
router.get('/', async (req, res) => {
    try {
        const events = await RentalEvent.find({})
        for (let i = 0; i < events.length; i++) {
            events[i].vehicle = await Vehicle.findOne({ _id: events[i].vehicle })
            events[i].vehicle.vehicleType = await VehicleType.findOne({ _id: events[i].vehicle.vehicleType })
            events[i].customer = await Customer.findOne({ _id: events[i].customer })
        }
        res.send(events)
        console.log(events)
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

// FOR CREATING A RENTAL EVENT
router.post('/create', async (req, res) => {
    // IF WERE TO GET THE DATA FROM A FORM 
    // const rentalEvent = new RentalEvent({
    //     startDate: req.body.startDate,
    //     endDate: req.body.endDate,
    //     vehicle: req.body.vehicle,
    //     customer: req.body.customer
    // })
    const createRentalEvent = async ({ startDate, endDate, vehicleModel, customer }) => {
        //    CHECKING THE VALIDITY OF DATES
        if (new Date(endDate) < new Date(startDate)) {
            return res.json({ message: 'Invalid Dates' })
        }

        try {
            const wantedCustomer = await Customer.findOne({ fullName: customer })
            const wantedVehicle = await Vehicle.findOne({ model: vehicleModel })
            wantedVehicle.vehicleType = await VehicleType.findOne({ _id: wantedVehicle.vehicleType })
            let discount = 0
            let customerBonusPeriod = null

            const rentalEvent = new RentalEvent({
                // startDate: new Date(startDate),
                endDate: new Date(endDate),
                vehicle: wantedVehicle,
                customer: wantedCustomer
            })

            let daysToBeUsed = new Date(
                rentalEvent.endDate.getTime() - rentalEvent.startDate.getTime()
            )

            if (wantedCustomer.lastRent) {
                let timeDifference = new Date(new Date().getTime() - new Date(wantedCustomer.lastRent).getTime())
                // 1 day has 86400000 miliseconds
                customerBonusPeriod = timeDifference.getTime() / 86400000 < 60 && wantedCustomer.rentedVehicles > 3
            } else {
                wantedCustomer.lastRent = rentalEvent.startDate
            }

            if (customerBonusPeriod) {
                discount = 15
            } else if (new Date(daysToBeUsed).getDate() > 10) {
                discount = 10
            } else if (new Date(daysToBeUsed).getDate() > 5) {
                discount = 7
            } else if (new Date(daysToBeUsed).getDate() > 3) {
                discount = 5
            }
            rentalEvent.price = (rentalEvent.vehicle.pricePerDay - (rentalEvent.vehicle.pricePerDay * discount) / 100).toFixed(2)
            wantedCustomer.lastRent = rentalEvent.startDate
            wantedCustomer.rentedVehicles++
            wantedVehicle.count--

            res.send(rentalEvent)
            await wantedCustomer.save()
            await wantedVehicle.save()
            await rentalEvent.save()
        } catch (err) {
            res.status(400).json({ message: err.message })
        }
    }
    createRentalEvent({
        startDate: new Date(),
        endDate: new Date('04 25 2021'),
        vehicleModel: 'C Class',
        customer: 'Dwayne Jonson'
    })
})
// FIND A SPECIFIC RENTAL EVENT
router.get('/:id', async (req, res) => {
    try {
        const event = await RentalEvent.findOne({ _id: req.params.id })
        event.vehicle = await Vehicle.findOne({ _id: event.vehicle })
        event.vehicle.vehicleType = await VehicleType.findOne({ _id: event.vehicle.vehicleType })
        event.customer = await Customer.findOne({ _id: event.customer })
        res.send(event)
        console.log(event)
    } catch (err) {
        res.status(404).json({ message: err.message })
    }
})

// FOR UPDATEING RENTAL EVENT
router.put('/:id/update', async (req, res) => {
    // const entries = Object.keys(req.body)
    // const updates = {}
    // for (let i = 0; i < entries.length; i ++) {
    //     updates[entries[i]] = Object.values(req.body)[i]
    // }
    // await RentalEvent.findByIdAndUpdate(req.params.id, updates, { new: true })
    try {
        await RentalEvent.findByIdAndUpdate(req.params.id, { customer: await Customer.findOne({ fullName: "Keisy Blase" }) })
        res.redirect(`/rentalEvent/${req.params.id}`)
    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

// FOR DELETING RENTAL EVENT
router.delete('/:id/delete', async (req, res) => {
    try {
        await RentalEvent.findByIdAndDelete(req.params.id)
        res.redirect('/rentalEvent')
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})
module.exports = router