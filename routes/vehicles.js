const express = require('express')
const Vehicle = require('../models/vehicle.js')
const VehicleType = require('../models/vehicleType.js')
const fetch = require('node-fetch')

const router = express.Router()

router.get('/', (req, res) => {
    res.redirect('/vehicle')
})
// FOR LIST OF ALL CARS
router.get('/vehicle', async (req, res) => {
    try {
        const vehicles = await Vehicle.find()
        for (let i = 0; i < vehicles.length; i++) {
            vehicles[i].vehicleType = await VehicleType.findOne({ _id: vehicles[i].vehicleType })
        }
        res.send(vehicles)

    }
    catch (err) {
        res.status(500).json({ message: err.message })
    }
})

// FOR CREATING A NEW CAR
router.post('/vehicle/create', (req, res) => {
    // IF WE ARE GETTING THE DATA FROM A FORM
    //     const car = new Vehicle({
    //         model: req.body.model,
    //         brand: req.body.brand,
    //         constructionDate: req.body.constructionDate,
    //         fuelType: req.body.fuelType,
    //         numberOfSeats: req.body.numberOfSeats,
    //         pricePerDay: req.body.pricePerDay,
    //         picture: req.body.picture,
    //         count: req.body.count,
    //     })
    // await car.save()
    const createVehicle = async ({ brand, model, constructionDate, fuelType, numberOfSeats, pricePerDay, picture, count }, vType) => {
        const car = new Vehicle({
            model: model,
            brand: brand,
            constructionDate: constructionDate,
            fuelType: fuelType,
            numberOfSeats: numberOfSeats,
            pricePerDay: pricePerDay,
            picture: picture,
            count: count,
        })
        try {
            car.vehicleType = await VehicleType.findOne({ name: vType })
            console.log(car)
            await car.save()
        } catch (err) {
            res.status(400).json({ message: err.message })
        }
    }
    createVehicle({
        model: 'Clio',
        brand: 'Renault',
        constructionDate: 2004,
        fuelType: 'Petrol',
        numberOfSeats: 5,
        pricePerDay: 15,
        picture: 'https://images.unsplash.com/photo-1605527072515-18d80e1433f5?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8Y2xpb3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
        count: 7
    }, 'Hatchback')
    res.redirect('/')
})


// FOR SPECIFIC CAR
router.get('/vehicle/:id', async (req, res) => {
    try {
        const vehicle = await Vehicle.findOne({ _id: req.params.id })
        vehicle.vehicleType = await VehicleType.findOne({ _id: vehicle.vehicleType })
        res.send(vehicle)
        console.log(vehicle)

    }
    catch (err) {
        res.status(404).json({ message: err.message })
    }
})

// FOR UPDATING A CAR
router.put('/vehicle/:id/update', async (req, res) => {
    // IF WE WOULD GET THE DATA FROM A FORM ->
    // const entries = Object.keys(req.body)
    // const updates = {}
    // for (let i = 0; i < entries.length; i ++) {
    //     updates[entries[i]] = Object.values(req.body)[i]
    // }
    // await Vehicle.findByIdAndUpdate(req.params.id, updates, { new: true })

    try {
        await Vehicle.findByIdAndUpdate(req.params.id, { count: 200, updatedAt: new Date }, { new: true })
        res.redirect(`/vehicle/${req.params.id}`)
    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

// FOR DELETING A CAR
router.delete('/vehicle/:id/delete', async (req, res) => {
    try {
        await Vehicle.findByIdAndDelete(req.params.id)
        res.redirect('/vehicle')
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

module.exports = router