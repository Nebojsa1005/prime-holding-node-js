const express = require('express')
const VehicleType = require('../models/vehicleType.js')

const router = express.Router()

// GET ALL VEHICLE TYPES
router.get('/', async (req, res) => {
    try {
        res.send(await VehicleType.find({}))
        console.log(await VehicleType.find({}))
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

// CREATE VEHICLE TYPE
router.post('/create', async (req, res) => {
    // IF WE ARE GETTING THE DATA FROM A FORM
    //     const newType = new VehicleType({
    //         createdAt: new Date(),
    //         name: req.body.name 
    //     })
    // await newType.save()

    const createType = async (name) => {
        const newType = new VehicleType({
            createdAt: new Date(),
            name: name
        })
        try {
            await newType.save()
        } catch (err) {
            res.status(400).json({ message: err.message })
        }
    }
    createType('Pickup Truck')
    res.redirect('/vehicleTypes')
})

// GET A SINGLE VEHICLE TYPE
router.get('/:id', async (req, res) => {
    try {
        const vehicleType = await VehicleType.findOne({ _id: req.params.id })
        res.send(vehicleType)
        console.log(vehicleType)
    } catch (err) {
        res.status(404).json({ message: err.message })
    }
})

// UPDATE VEHICLE TYPE
router.put('/:id/update', async (req, res) => {
    // IF WE WOULD GET THE DATA FROM A FORM ->
    // const entries = Object.keys(req.body)
    // const updates = {}
    // for (let i = 0; i < entries.length; i ++) {
    //     updates[entries[i]] = Object.values(req.body)[i]
    // }
    // await VehicleType.findByIdAndUpdate(req.params.id, updates, { new: true })
    try {
        await VehicleType.findByIdAndUpdate(req.params.id, { name: 'Hatchback', updatedAt: new Date }, { new: true })
        res.redirect(`/vehicleTypes/${req.params.id}`)

    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

// DELETE A VEHICLE TYPE
router.delete('/:id/delete', async (req, res) => {
    try {
        await VehicleType.findByIdAndDelete(req.params.id)
        res.redirect('/vehicleTypes')
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})


module.exports = router