const express = require('express')
const Customer = require('../models/customer.js')
const router = express.Router()

// FOR LIST OF ALL CUSTOMERS
router.get('/', async (req, res) => {
    try {
        const customer = await Customer.find()
        res.send(customer)

    }
    catch (err) {
        res.status(500).json({ message: err.message })
    }
})

// FOR CREATING A NEW CUSTOMER
router.post('/create', (req, res) => {
    // IF WE ARE GETTING THE DATA FROM A FORM
    //     const customer = new Customer({
    //         createdAt: new Date,
    //          updatedAt: req.body.updatedAt,
    //          fullName: req.body.fullName,
    //          emailAddress: req.body.emailAddress,
    //          phoneNumber: req.body.phoneNumber

    //     })
    // await customer.save()
    const createCustomer = async ({ createdAt, fullName, emailAddress, phoneNumber }, vType) => {
        const customer = new Customer({
            createdAt: createdAt,
            fullName: fullName,
            emailAddress: emailAddress,
            phoneNumber: phoneNumber
        })
        try {
            console.log(customer)
            await customer.save()
        } catch (err) {
            res.status(400).json({ message: err.message })
        }
    }
    createCustomer({
        createdAt: new Date,
        fullName: 'Dwayne Jonson',
        emailAddress: 'dwayne@dwayne.com',
        phoneNumber: '06132225555'
    })
    res.redirect('/customers')
})


// FOR SPECIFIC CUSTOMER
router.get('/:id', async (req, res) => {
    try {
        const customer = await Customer.findOne({ _id: req.params.id })
        res.send(customer)
        console.log(customer)
    }
    catch (err) {
        res.status(404).json({ message: err.message })
    }
})
// FOR UPDATING A CUSTOMER
router.put('/:id/update', async (req, res) => {
    // IF WE WOULD GET THE DATA FROM A FORM ->
    // const entries = Object.keys(req.body)
    // const updates = {}
    // for (let i = 0; i < entries.length; i ++) {
    //     updates[entries[i]] = Object.values(req.body)[i]
    // }
    // await Customer.findByIdAndUpdate(req.params.id, updates, { new: true })
    try {
        await Customer.findByIdAndUpdate(req.params.id, { fullName: 'Pedja Stojakovic', updatedAt: new Date }, { new: true })
        res.redirect(`/customers/${req.params.id}`)
    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

// FOR DELETING A CUSTOMER
router.delete('/:id/delete', async (req, res) => {
    try {
        await Customer.findByIdAndDelete(req.params.id)
        res.redirect('/customers')
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

module.exports = router